package com.countree;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URLConnection;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlSerializer;

import android.os.Environment;
import android.os.Message;
import android.util.Log;
import android.util.Xml;
import android.widget.Toast;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

public class Communication {
    String URL = "http://imedia-ftp.inria.fr:8080/Identification_new-backup/rest/IdentificationService";  
    String result = "";  
    String deviceId = "xxxxx" ;  
    final String tag = "Your Logcat tag: ";  

    
    
    
    public String creerxmlpackage (List<String> label_photo, List<File> photos)
    {
        XmlSerializer serializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();
        try {
            serializer.setOutput(writer);
            serializer.startDocument("UTF-8", true);
            serializer.startTag("", "photos");
            serializer.attribute("", "number", String.valueOf(label_photo.size()));
            int i=0;
            Bitmap bmpimage;
            ByteArrayOutputStream compressedimage = new ByteArrayOutputStream();
            while (i!=label_photo.size())
            {
            	bmpimage=BitmapFactory.decodeFile(photos.get(i).getPath(),null);
            	serializer.startTag("", "image");
            	serializer.attribute("", "label", label_photo.get(i));
            	bmpimage.compress(CompressFormat.JPEG, 100, compressedimage);
            	byte[] data = compressedimage.toByteArray();
            	String Strimage = data.toString();
            	serializer.attribute("", "img", Strimage);
            	serializer.endTag("", "image");          
            	i++;
            }
            serializer.endTag("", "photos");
            serializer.endDocument();
            return writer.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } 
    }   
    
    
    public static void upload_file(String path) throws MalformedURLException, IOException{


        BufferedInputStream bis = null;
        BufferedOutputStream  bos = null;
        try
        {

           java.net.URL url = new java.net.URL("/*url serveur*/");

           URLConnection urlc = url.openConnection();

           System.out.println("Connection established:");

               bos = (BufferedOutputStream) urlc.getOutputStream(); 

               bis = new BufferedInputStream(new FileInputStream(path));
                 byte[] readData = new byte[1024];
                 int length;
                 while((length=bis.read(readData))>0) {
                   bos.write(readData);
                 }
                 bis.close();
                 bos.flush();
                 bos.close();
        }catch(IllegalStateException ise){  System.out.println("Illegal state exception for setUseCaches.."); }
        finally
        {
           if (bis != null)
              try{
                 bis.close();
                 System.out.println("closed bis");
              }
              catch (IOException ioe){}
           if (bos != null)
              try{
                 bos.close();
                 System.out.println("closed bos");
              }catch (IOException ioe){                   
                   System.out.println("Exception, not finally..");
              }
        }
     }
    
    //http://imedia-ftp.inria.fr:8080/Identification_new-backup/rest/IdentificationService/Taxons?external_image_urls=
    //lien vers image
    //&db_name=fruit/fleur/plantscan/
    
    /*public void callWebService(String q)
    {  
        HttpClient httpclient = new DefaultHttpClient();  
        HttpGet request = new HttpGet(URL + q);  
        request.addHeader("deviceId", deviceId);  
        ResponseHandler<String> handler = new BasicResponseHandler();  
        try {  
            result = httpclient.execute(request, handler);  
        } catch (ClientProtocolException e) {  
            e.printStackTrace();  
        } catch (IOException e) {  
            e.printStackTrace();  
        }  
        httpclient.getConnectionManager().shutdown();  
        Log.i(tag, result);  
    }*/
    
    
  //Si les logs sont corrects :
    //Cr�ation du fichier xml contenant les images
    /*
    public void creerxmlpackage2 (List<String> label_photo, List<File> photos)
    {
    File newxmlfile = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/donnes_a_envoyer_shazarbre.xml");
    try{
           newxmlfile.createNewFile();
       }catch(IOException e)
       {
           Log.e("IOException", "Exception in create new File(");
       }
       FileOutputStream fileos = null;
       try{
           fileos = new FileOutputStream(newxmlfile);

       }catch(FileNotFoundException e)
       {
           Log.e("FileNotFoundException",e.toString());
       }
       XmlSerializer serializer = Xml.newSerializer();
       try{
       serializer.setOutput(fileos, "UTF-8");
       serializer.startDocument(null, Boolean.valueOf(true));
       serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
       serializer.startTag(null, "root");
       int i =0;
       while (i!=label_photo.size())
       {
       	serializer.startTag(null, "picture");
       	serializer.attribute(null, "label", label_photo.get(i).toString());
       	serializer.attribute(null, "photo", photos.get(i).toString());
       	serializer.endTag("", "picture");          
       	i++;
       }

       serializer.endTag(null,"root");
       serializer.endDocument();
       serializer.flush();
       fileos.close();

       }catch(Exception e)
       {
           Log.e("Exception","Exception occured in writing");
       }

}*/
}
