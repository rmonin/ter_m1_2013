package com.countree;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLConnection;

public class Transfert_xml_plantnet_to_client {
    public static void main (File xmlaparser, String adresse) throws 
IOException
    {
        BufferedInputStream bis = null;
        BufferedOutputStream  bos = null;
        try
        {

           java.net.URL url = new java.net.URL(adresse);

           URLConnection urlc = url.openConnection();

           System.out.println("Connection established:");

               bos = (BufferedOutputStream) urlc.getOutputStream(); 

               bis = new BufferedInputStream(new FileInputStream(xmlaparser));
                 byte[] readData = new byte[1024];
                 int length;
                 while((length=bis.read(readData))>0) {
                   bos.write(readData);
                 }
                 bis.close();
                 bos.flush();
                 bos.close();
        }
        catch(IllegalStateException ise){  System.out.println("Illegal state exception for setUseCaches.."); }
        finally
        {
           if (bis != null)
              try{
                 bis.close();
                 System.out.println("closed bis");
              }
              catch (IOException ioe){}
           if (bos != null)
              try{
                 bos.close();
                 System.out.println("closed bos");
              }catch (IOException ioe){                   
                   System.out.println("Exception, not finally..");
              }
        }
    }
}
