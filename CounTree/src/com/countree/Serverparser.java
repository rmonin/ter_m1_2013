package com.countree;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.util.Base64;
import android.widget.ImageView;


public class Serverparser {

	
    public static String ParseXMLclient (File xmlaparser)//parse le xml envoy� par le client et genere l'url � envoyer au serveur
            throws XmlPullParserException, IOException
        {
    	String url = "http://imedia-ftp.inria.fr:8080/Identification_new-backup/rest/IdentificationService/Taxons?external_image_urls=";
    	String labels[]=null; 
    	String filenames[]=null;
    	int i=0;
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser xpp = factory.newPullParser();
            
            InputStream stream =  new FileInputStream(xmlaparser); 
            xpp.setInput(stream, null);
            
            
            int eventType = xpp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
             if(eventType == XmlPullParser.START_DOCUMENT) {
                eventType = xpp.next();
             } else if(eventType == XmlPullParser.START_TAG) {
                System.out.println("Start tag "+xpp.getName());
                if (xpp.getName()=="image")
                {
                	labels[i]=xpp.getAttributeValue(0);
                    File fichier = new File("/*url*/");
                    fichier.createNewFile();
        			FileWriter fw = new FileWriter(fichier.getAbsoluteFile());
        			BufferedWriter bw = new BufferedWriter(fw);
        			bw.write(xpp.getAttributeValue(1));
        			bw.close();
                    filenames[i]=fichier.getPath();             
                	i++;
                }
             } else if(eventType == XmlPullParser.END_TAG) {
                 eventType = xpp.next();

             } else if(eventType == XmlPullParser.TEXT) {
                 eventType = xpp.next();

             }
             eventType = xpp.next();
            }
            
            for (int x=0;x < filenames.length-1;x++)
            {
            	url=url+filenames[x]+",";
            }
            url=url+filenames[filenames.length]+"&db_name=";
            
            for (int y=0;y < labels.length-1;y++)
            {
            	url=url+labels[y]+",";
            }
            url=url+labels[labels.length];
            
            
            return url;

        }
	
}
