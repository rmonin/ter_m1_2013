package com.countree;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;



public class XMLparsing {
	static String name[] = null;
	static String identification_rate[] = null;
	static String url_image[][]=null;
    static String url_fiche[][] = null;
    static String famille[]=null;
    static String genre[]=null;
    static String nom_retenu[]=null;

    public static void main (File xmlaparser) //parse l'xml renvoy� par le serveur
        throws XmlPullParserException, IOException
    {
/*<plantunit id="000073366L.jpg" image="http://www.tela-botanica.org/appli:cel-img:000073366L.jpg" 
 * miniature="http://www.tela-botanica.org/appli:cel-img:000073366CS.jpg" 
 * url_fiche="http://www.tela-botanica.org/eflore/BDNFF/4.02/nn/75048" auteur_image="Herv� GO�AU"
 *  famille="Fabaceae" genre="Cercis" nom_retenu="Cercis siliquastrum L." variete="Cercis siliquastrum
 *   L." localite="Puich�ric (11301)  Canal du Midi" organe="fleur" num_observation="749059" date_observation=
 *   "mardi 17 avril 2012" date_indexation="2013-02-19 10:18:50" date_creation="mardi 15 mai 2012 � 15:43"
 *    Remarques="null"/>*/
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();

        InputStream stream =  new FileInputStream(xmlaparser); 
        xpp.setInput(stream, null);
        
        int cpt_taxon=0;
        int cpt_images=0;

        xpp.setInput( new StringReader ( "<foo>Hello World!</foo>" ) );
        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
         if(eventType == XmlPullParser.START_DOCUMENT) {
         } else if(eventType == XmlPullParser.START_TAG) {
        	 if (xpp.getName()=="taxon")//nouvelle entr�e de type d'arbre
        	 {
        		 name[cpt_taxon]=xpp.getAttributeValue(0);
        		 identification_rate[cpt_taxon]=xpp.getAttributeValue(3);


        		 cpt_taxon++;
        	 }
        	 else if (xpp.getName()=="plantunit")//nouvelle instance d'un type d'arbre
        	 {
        		url_image[cpt_taxon][cpt_images]=xpp.getAttributeValue(1);
        		url_fiche[cpt_taxon][cpt_images]=xpp.getAttributeValue(3);
       		 	famille[cpt_taxon]=xpp.getAttributeValue(5);
       		 	genre[cpt_taxon]=xpp.getAttributeValue(6);
       		 	nom_retenu[cpt_taxon]=xpp.getAttributeValue(7);
        		cpt_images++;
        	 }
         } else if(eventType == XmlPullParser.END_TAG) {
         } else if(eventType == XmlPullParser.TEXT) {
         }
         eventType = xpp.next();
        }
        System.out.println("End document");
    }

    public String get_identification_rate(int i)
    {
    	return identification_rate[i];
    }

    public String get_famille(int i)
    {
    	return famille[i];
    }
    public String get_genre(int i)
    {
    	return genre[i];
    }
    public String get_name(int i)
    {
    	return name[i];
    }
    public String get_url_image(int i, int j)
    {
    	return url_image[i][j];
    }
    public String get_url_fiche(int i, int j)
    {
    	return url_fiche[i][j];
    }
	
}
